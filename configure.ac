AC_INIT(qwitter, 0.1.0)
AC_PREREQ(2.59)
AC_CONFIG_SRCDIR(README)
AM_INIT_AUTOMAKE([foreign dist-bzip2])
AM_MAINTAINER_MODE

GNOME_COMMON_INIT
GNOME_DOC_INIT

AC_CONFIG_HEADER(config.h)
AC_CONFIG_MACRO_DIR(m4)

m4_pattern_allow([^BOOST_])

MAJOR_VERSION=0
MINOR_VERSION=1
MICRO_VERSION=0

QWITTER_VERSION="$MAJOR_VERSION.$MINOR_VERSION.$MICRO_VERSION"
AC_SUBST(QWITTER_VERSION)


dnl all the library version.
dnl if one is harcoded elsewhere, it is a bug

GTK_VERSION=2.12
LIBGLIBMM_VERSION=2.0
LIBGTKMM_VERSION=2.12.0
LIBGCONF_VERSION=2.0.0
GTKSPELL_VERSION=2.0.9
BOOST_VERSION=1.34
DBUSCPP_VERSION=0.5.0

AC_PROG_CXX
AC_GNU_SOURCE

AC_ARG_ENABLE(debug,[  --enable-debug    Turn on debugging],[
        case "${enableval}" in
           yes) debug=true ;
                DEBUG_CFLAGS="-DDEBUG -g" ;
                OPTIMIZE_CFLAGS="" ;;
            no) debug=false ;
                DEBUG_CFLAGS="-DNDEBUG" ;;
             *) AC_MSG_ERROR(bad value ${enableval} for --enable-debug) ;;
        esac
],[     debug=false
        DEBUG_CFLAGS="-DNDEBUG"
])
AM_CONDITIONAL(DEBUG, test x$debug = xtrue)


CPPFLAGS="$CPPFLAGS $DEBUG_CFLAGS -Wall -Wcast-align -Wcast-qual -Wpointer-arith -Wreturn-type"
CFLAGS="$CFLAGS $DEBUG_CFLAGS $OPTIMIZE_CFLAGS"
CXXFLAGS="$CXXFLAGS $DEBUG_CFLAGS $OPTIMIZE_CFLAGS"
dnl CFLAGS=""
LDFLAGS="$LDFLAGS"


dnl AC_PROG_INSTALL
AC_PROG_LIBTOOL

AC_LANG_CPLUSPLUS
AC_LANG_COMPILER_REQUIRE

PKG_CHECK_MODULES(LIBGLIBMM, [glibmm-2.4 gthread-2.0])
PKG_CHECK_MODULES(GTK, [gtk+-2.0 >= $GTK_VERSION])
PKG_CHECK_MODULES(LIBGTKMM, [gtkmm-2.4 >= $LIBGTKMM_VERSION])
PKG_CHECK_MODULES(LIBXML, [libxml-2.0])
dnl PKG_CHECK_MODULES(LIBXSLT, [libxslt])
PKG_CHECK_MODULES(GCONF, [gconf-2.0])
PKG_CHECK_MODULES(LIBSOUP, [libsoup-2.4])
dnl PKG_CHECK_MODULES(PCRE, [libpcrecpp])

PKG_CHECK_EXISTS(gtk+-2.0 >= 2.14.0,
  [AC_DEFINE(HAVE_GTK_SHOW_URI, 1, [Define to 1 if you have gtk_show_uri])],
  [])
PKG_CHECK_EXISTS(gtkmm-2.4 >= 2.14.0,
  [AC_DEFINE(HAVE_CLASS_GTK__WIDGET_SIGNAL_POPUP_MENU, 1, [Define to 1 if class Gtk::Widget has signal_popup_menu])],
  [])


BOOST_REQUIRE([$BOOST_VERSION])
BOOST_BIND
BOOST_CONVERSION
BOOST_FILESYSTEM
BOOST_FORMAT
BOOST_TEST([s])
AC_CHECK_HEADER(tr1/memory,,[
	CXXFLAGS="$CXXFLAGS -I/usr/include/boost/tr1";
	AC_MSG_NOTICE([using boost tr1 implementation.])
])


AC_LANG_PUSH(C++)
if test "$GCC" = "yes"; then
        QWITTER_BUILD_CONFIG="$QWITTER_BUILD_CONFIG gcc-options="
        for option in -Wall -Wextra -Wsign-compare -Wpointer-arith \
                      -Wchar-subscripts -Wwrite-strings \
                      -Wunused -Wpointer-arith -Wshadow -fshow-column ; do
                SAVE_CXXFLAGS="$CXXFLAGS"
                CXXFLAGS="$CXXFLAGS $option"
		QWITTER_BUILD_CONFIG="$QWITTER_BUILD_CONFIG$option "
                AC_MSG_CHECKING([whether gcc understands $option])
                AC_TRY_COMPILE([], [],
                        has_option=yes,
                        has_option=no,)
                if test $has_option = no; then
                  CXXFLAGS="$SAVE_CXXFLAGS"
                fi
                AC_MSG_RESULT($has_option)
                unset has_option
                unset SAVE_CXXFLAGS
        done
        unset option
fi
AC_LANG_POP

IT_PROG_INTLTOOL([0.35.0])

GETTEXT_PACKAGE=qwitter
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE], ["$GETTEXT_PACKAGE"],
                   [The domain to use with gettext])
AM_GLIB_GNU_GETTEXT

#
# Find gconftool-2
#
AC_PATH_PROG(GCONFTOOL, gconftool-2)
AM_GCONF_SOURCE_2

QWITTER_LOCALEDIR=[${datadir}/locale]
AC_SUBST(QWITTER_LOCALEDIR)

AC_DEFINE_UNQUOTED([QWITTER_LOCALEDIR], ["$QWITTER_LOCALEDIR"], [The locale directory.])

AC_DEFINE_UNQUOTED([QWITTER_BUILD_CONFIG], ["$QWITTER_BUILD_CONFIG"], [The string used to hardcode the build config.])

AC_CONFIG_FILES([
Makefile
src/Makefile
po/Makefile.in
po/Makefile
])


AC_OUTPUT
