/*
 * qwitter
 *
 * Copyright (C) 2009 Hubert Figuiere
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm/i18n.h>
#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/stock.h>

#include "clientwindow.hpp"
#include "microb/client.hpp"
#include "microb/context.hpp"

namespace qwitter 
{


static const char * ui =
  "<ui>"
  "  <menubar name='MenuBar'>"
  "    <menu action='MenuQwitter'>"
  "      <menuitem action='Close'/>"
  "    </menu>"
  "  </menubar>"
  "</ui>";


ClientWindow::ClientWindow()
  : Gtk::Window(Gtk::WINDOW_TOPLEVEL)
  , m_layout(NULL)
  , m_tabs(NULL)
  , m_client(NULL)
  , m_context(NULL)
{
  set_title(_("Qwitter"));

  Glib::RefPtr<Gtk::Builder> builder 
    = Gtk::Builder::create_from_file("clientwindow.ui", "layout");

  add(m_root_content);

  builder->get_widget("layout", m_layout);
  
  builder->get_widget("tabs", m_tabs);

  m_actions = Gtk::ActionGroup::create();

  m_actions->add(Gtk::Action::create("MenuQwitter", _("_Qwitter")));
  m_actions->add(Gtk::Action::create("Close", Gtk::Stock::CLOSE));

  m_manager = Gtk::UIManager::create();
  m_manager->insert_action_group(m_actions);
  m_manager->add_ui_from_string(ui);
  Gtk::Widget* menuBar = m_manager->get_widget("/MenuBar");

  m_root_content.pack_start(*menuBar, Gtk::PACK_SHRINK);
  m_root_content.pack_start(*m_layout);

  Gtk::Button * send_button;
  Gtk::Button * cancel_button;
  builder->get_widget("send", send_button);
  builder->get_widget("cancel", cancel_button);
  builder->get_widget("input", m_input);

  send_button->signal_clicked().connect(sigc::mem_fun(*this, &ClientWindow::send_message));
  cancel_button->signal_clicked().connect(sigc::mem_fun(*this, &ClientWindow::cancel_message));


  m_accounts = load_accounts();
  m_client = new MicroBClient(m_accounts);

  show_all();
}

ClientWindow::~ClientWindow()
{
  delete m_client;
}

void ClientWindow::add_tab(Gtk::Widget * w, const Glib::ustring & label)
{
  m_tabs->append_page(*w, label);
}


MicroBAccounts::Ptr ClientWindow::load_accounts()
{
  MicroBAccounts::Ptr accounts(new MicroBAccounts);
  
  return accounts;
}

void ClientWindow::cancel_message()
{
  m_input->delete_text(0, -1);
  reset_context();
}


void ClientWindow::send_message()
{
  Glib::ustring message = m_input->get_chars(0, -1);
  m_client->send_message(message, m_context);
  m_input->delete_text(0, -1);  
  reset_context();
}


void ClientWindow::reset_context()
{
  if(m_context) {
    delete m_context;
  }
}

}
