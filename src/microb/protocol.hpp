/*
 * qwitter
 *
 * Copyright (C) 2009 Hubert Figuiere
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PROTO_PROTOCOL_HPP_
#define __PROTO_PROTOCOL_HPP_

#include <string>

#include <sigc++/signal.h>

namespace MicroB {

class Protocol
{
public:
  virtual ~Protocol() {}

  /** login with user and password. Return true if success. */
  virtual bool login(const std::string & user, const std::string & password) = 0;
  
  /** get the timelime from all followed */
  virtual bool get_home_timeline() = 0;
  /** get a specific user timeline */
  virtual bool get_user_timeline(const std::string & user) = 0;
  /** get the public timeline */
  virtual bool get_public_timeline() = 0;
  
  
protected:

  sigc::signal<void, MicroB::Feed::Ptr> signal_home_timeline_loaded;
  sigc::signal<void, MicroB::Feed::Ptr> signal_user_timeline_loaded;
  sigc::signal<void, MicroB::Feed::Ptr> signal_public_timeline_loaded;

  sigc::signal<void, std::string> signal_status_message;
  sigc::signal<void, bool> signal_on_connect;

};


}

#endif
