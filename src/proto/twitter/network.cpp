/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 2007-2009 Daniel Morales <daniminas@gmail.com>
 * Copyright (C) 2009 Hubert Figuiere
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copied from Twitux - Authors: Daniel Morales <daniminas@gmail.com>
 *
 */

#include "config.h"

#include <string.h>

#include <boost/format.hpp>

#include <glib.h>
#include <glibmm/i18n.h>

#ifdef HAVE_GNOME_KEYRING
#include <libtwitux/twitux-keyring.h>
#endif

#include "proto/twitter/twitux.h"
#include "proto/twitter/debug.h"
#include "proto/twitter/conf.h"
#include "proto/twitter/network.h"
#include "proto/twitter/parser.h"
//#include "twitux-app.h"
//#include "twitux-send-message-dialog.h"
//#include "twitux-lists-dialog.h"

namespace MicroB {


#define DEBUG_DOMAIN	  "Network"
#define TWITUX_HEADER_URL "http://gitorious.org/qwitter"

struct TwituxImage {
	gchar        *src;
//	GtkTreeIter   iter;
};


#define G_STR_EMPTY(x) ((x) == NULL || (x)[0] == '\0')

// 



/* This function must be called at startup */
TwitterNetwork::TwitterNetwork(void)
	: soup_connection(NULL)
	, processing(FALSE)
	, current_timeline(NULL)
	, timeout_id(0)
{
	TwituxConf	*conf;
	gboolean	check_proxy = FALSE;
 
	/* Async connections */
	soup_connection = soup_session_async_new_with_options (SOUP_SESSION_MAX_CONNS,
														   8,
														   NULL);

	twitux_debug (DEBUG_DOMAIN, "Libsoup (re)started");

	/* Set the proxy, if configuration is set */
	conf = twitux_conf_get ();
	twitux_conf_get_bool (conf,
						  TWITUX_PROXY_USE,
						  &check_proxy);

	if (check_proxy) {
		gchar *server, *proxy_uri;
		gint port;

		/* Get proxy */
		twitux_conf_get_string (conf,
								TWITUX_PROXY_HOST,
								&server);
		twitux_conf_get_int (conf,
							 TWITUX_PROXY_PORT,
							 &port);

		if (server && server[0]) {
			SoupURI *suri;

			check_proxy = FALSE;
			twitux_conf_get_bool (conf,
								  TWITUX_PROXY_USE_AUTH,
								  &check_proxy);

			/* Get proxy auth data */
			if (check_proxy) {
				char *user, *password;

				twitux_conf_get_string (conf,
										TWITUX_PROXY_USER,
										&user);
				twitux_conf_get_string (conf,
										TWITUX_PROXY_PASS,
										&password);

				proxy_uri = g_strdup_printf ("http://%s:%s@%s:%d",
											 user,
											 password,
											 server,
											 port);

				g_free (user);
				g_free (password);
			} else {
				proxy_uri = g_strdup_printf ("http://%s:%d", 
											 server, port);
			}

			twitux_debug (DEBUG_DOMAIN, "Proxy uri: %s",
						  proxy_uri);

			/* Setup proxy info */
			suri = soup_uri_new (proxy_uri);
			g_object_set (G_OBJECT (soup_connection),
						  SOUP_SESSION_PROXY_URI,
						  suri,
						  NULL);

			soup_uri_free (suri);
			g_free (server);
			g_free (proxy_uri);
		}
	}
}


/* Cancels requests, and unref libsoup. */
TwitterNetwork::~TwitterNetwork(void)
{
	/* Close all connections */
	stop ();

	g_object_unref (soup_connection);
	soup_connection = NULL;

	if (current_timeline) {
		g_free (current_timeline);
		current_timeline = NULL;
	}

	twitux_debug (DEBUG_DOMAIN, "Libsoup closed");
}


/* Cancels all pending requests in session. */
void
TwitterNetwork::stop	(void)
{
	twitux_debug (DEBUG_DOMAIN,"Cancelled all connections");

	soup_session_abort (soup_connection);
}


/* Login in Twitter */
void
TwitterNetwork::login (const char *username, const char *password)
{
	twitux_debug (DEBUG_DOMAIN, "Begin login.. ");

	m_username = username;
	m_password = password;

	signal_status_message(_("Connecting..."));

	/* HTTP Basic Authentication */
	g_signal_connect (soup_connection,
					  "authenticate",
					  G_CALLBACK (cb_on_auth),
					  this);

	/* Verify credentials */
	get_data (TWITUX_API_LOGIN, cb_on_login, this);
}


/* Logout current user */
void TwitterNetwork::logout (void)
{
//	twitux_network_new ();
	
	twitux_debug (DEBUG_DOMAIN, "Logout");
}


/* Post a new tweet - text must be Url encoded */
void
TwitterNetwork::post_status (const gchar *text)
{
	gchar *formdata;

	formdata = g_strdup_printf ("source=twitux&status=%s", text);

	post_data (TWITUX_API_POST_STATUS,
					   formdata,
					   cb_on_post,
					   this);
}


/* Send a direct message to a follower - text must be Url encoded  */
void
TwitterNetwork::send_message (const gchar *frnd,
							 const gchar *text)
{
	gchar *formdata;

	formdata = g_strdup_printf ( "user=%s&text=%s", frnd, text);
	
	post_data (TWITUX_API_SEND_MESSAGE,
					   formdata,
					   cb_on_message,
					   this);
}

void
TwitterNetwork::refresh (void)
{
	if (!current_timeline || processing)
		return;

	/* UI */
	signal_status_message(_("Loading timeline..."));

	processing = TRUE;
	get_data (current_timeline, cb_on_timeline, new TimelineArg(this, NULL));
}

/* Get and parse a timeline */
void
TwitterNetwork::get_timeline (const gchar *url_timeline)
{
	if (processing)
		return;

	parser_reset_lastid ();

	/* UI */
	 signal_status_message(_("Loading timeline..."));

	processing = TRUE;
	get_data (url_timeline, cb_on_timeline, new TimelineArg(this, g_strdup(url_timeline)));
}

/* Get a user timeline */
void
TwitterNetwork::get_user (const gchar *username)
{
	gchar *user_timeline;
	gchar *user_id;

	if (!username){
		twitux_conf_get_string (twitux_conf_get (),
								TWITUX_PREFS_AUTH_USER_ID,
								&user_id);
	} else {
		user_id = g_strdup (username);
	}

	if(!G_STR_EMPTY (user_id)) {
		user_timeline = g_strdup_printf (TWITUX_API_TIMELINE_USER,
										 user_id);
	
		get_timeline (user_timeline);
		g_free (user_timeline);
	}

	if (user_id)
		g_free (user_id);
}

/* Get authenticating user's friends(following)
 * Returns:
 * 		NULL: Friends will be fetched
 * 		UserListPtr: The list of friends (fetched previously)
 */
UserListPtr
TwitterNetwork::get_friends ()
{
	if (user_friends)
		return user_friends;
	
	get_data (TWITUX_API_FOLLOWING,
					  TwitterNetwork::cb_on_friends,
					  this);
	
	return UserListPtr();
}


/* Get the authenticating user's followers
 * Returns:
 * 		NULL: Followers will be fetched
 * 		GList: The list of friends (fetched previously)
 */
UserListPtr
TwitterNetwork::get_followers (void)
{
	if (user_followers)
		return user_followers;

	get_data (TWITUX_API_FOLLOWERS,
					  TwitterNetwork::cb_on_followers,
					  this);

	return UserListPtr();
}


/* Get an image from servers */
void
TwitterNetwork::get_image (const gchar  *url_image) const
//						  GtkTreeIter   iter)
{
	gchar	*image_file;
	const gchar   *image_name;

	TwituxImage *image;

	/* save using the filename */
	image_name = strrchr (url_image, '/');
	if (image_name && image_name[1] != '\0') {
		image_name++;
	} else {
		image_name = "twitux_unknown_image";
	}

	image_file = g_build_filename (g_get_home_dir(), ".gnome2",
								   TWITUX_CACHE_IMAGES,
								   image_name, NULL);

	/* check if image already exists */
	if (g_file_test (image_file, (GFileTest)(G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))) {		
		/* Set image from file here */
//		twitux_app_set_image (image_file, iter);
		g_free (image_file);
		return;
	}

	image = g_new0 (TwituxImage, 1);
	image->src  = g_strdup (image_file);
//	image->iter = iter;

	g_free (image_file);

	/* Note: 'image' will be freed in 'network_cb_on_image' */
	get_data (url_image, TwitterNetwork::cb_on_image, new ImageArg(this, image));
}



/* Add a user to follow */
void
TwitterNetwork::add_user (const gchar *username)
{
	gchar *url;
	
	if (G_STR_EMPTY (username))
		return;
	
	url = g_strdup_printf (TWITUX_API_FOLLOWING_ADD, username);

	post_data (url, NULL, TwitterNetwork::cb_on_add, this);

	g_free (url);
}


/* Add a user to follow */
void
TwitterNetwork::del_user (const User::Ptr & user)
{
	std::string url;
	
	if (!user || user->screen_name.empty())
		return;
	
	url = str(boost::format(TWITUX_API_FOLLOWING_DEL) % user->screen_name);

	post_data (url, NULL, TwitterNetwork::cb_on_del, new DelArg(this, user));
}


/* Get data from net */
void
TwitterNetwork::get_data (const std::string & url,
				  SoupSessionCallback    callback,
				  gpointer               data) const
{
	SoupMessage *msg;

	twitux_debug (DEBUG_DOMAIN, "Get: %s",url.c_str());

	msg = soup_message_new ( "GET", url.c_str() );

	soup_session_queue_message (soup_connection, msg, callback, data);
}


/* Private: Post data to net */
void
TwitterNetwork::post_data (const std::string & url,
				   gchar                 *formdata,
				   SoupSessionCallback    callback,
				   gpointer               data) const
{
	SoupMessage *msg;

	twitux_debug (DEBUG_DOMAIN, "Post: %s",url.c_str());

	msg = soup_message_new ("POST", url.c_str());
	
	soup_message_headers_append (msg->request_headers,
								 "X-Twitter-Client", PACKAGE_NAME);
	soup_message_headers_append (msg->request_headers,
								 "X-Twitter-Client-Version", PACKAGE_VERSION);
	soup_message_headers_append (msg->request_headers,
								 "X-Twitter-Client-URL", TWITUX_HEADER_URL);

	if (formdata)
	{
		soup_message_set_request (msg, 
								  "application/x-www-form-urlencoded",
								  SOUP_MEMORY_TAKE,
								  formdata,
								  strlen (formdata));
	}

	soup_session_queue_message (soup_connection, msg, callback, data);
}


/* Check HTTP response code */
gboolean
TwitterNetwork::check_http (gint status_code) const
{
	if (status_code == 401) {
		signal_status_message (_("Access denied."));

	} else if (SOUP_STATUS_IS_CLIENT_ERROR (status_code)) {
		signal_status_message (_("HTTP communication error."));

	} else if(SOUP_STATUS_IS_SERVER_ERROR (status_code)) {
		signal_status_message (_("Internal server error."));

	} else if (!SOUP_STATUS_IS_SUCCESSFUL (status_code)) {
		signal_status_message (_("Stopped"));

	} else {
		return TRUE;
	}
	
	return FALSE;
}


/* HTTP Basic Authentication */
void
TwitterNetwork::cb_on_auth (SoupSession  * /*session*/,
							SoupMessage  * /*msg*/,
							SoupAuth     *auth,
							gboolean      /*retrying*/,
							gpointer      data)
{
	TwitterNetwork * self = (TwitterNetwork*)data;
	/* Don't bother to continue if there is no user_id */
	if (self->m_username.empty()) {
		return;
	}

	/* verify that the password has been set */
	if (!self->m_password.empty()) {
		soup_auth_authenticate (auth, self->m_username.c_str(), 
								self->m_password.c_str());
	}
}


/* On verify credentials */
void
TwitterNetwork::cb_on_login (SoupSession * /*session*/,
							 SoupMessage *msg,
							 gpointer     user_data)
{
	TwitterNetwork * self = (TwitterNetwork*)user_data;

	twitux_debug (DEBUG_DOMAIN,
				  "Login response: %i",msg->status_code);

	if (self->check_http (msg->status_code)) {
		self->signal_on_connect(true);
		return;
	}

	self->signal_on_connect(false);
}


/* On post a tweet */
void
TwitterNetwork::cb_on_post (SoupSession * /*session*/,
							SoupMessage *msg,
							gpointer     user_data)
{
	TwitterNetwork * self = (TwitterNetwork*)user_data;

	if (self->check_http (msg->status_code)) {
		self->signal_status_message (_("Status Sent"));
	}
	
	twitux_debug (DEBUG_DOMAIN,
				  "Tweet response: %i",msg->status_code);
}


/* On send a direct message */
void
TwitterNetwork::cb_on_message (SoupSession * /*session*/,
							   SoupMessage *msg,
							   gpointer     user_data)
{
	TwitterNetwork *self = (TwitterNetwork*)user_data;

	if (self->check_http (msg->status_code)) {
		self->signal_status_message (_("Message Sent"));
	}

	twitux_debug (DEBUG_DOMAIN,
				  "Message response: %i",msg->status_code);
}


/* On get a timeline */
void
TwitterNetwork::cb_on_timeline (SoupSession * /*session*/,
								SoupMessage *msg,
								gpointer     user_data)
{
	TimelineArg *arg = (TimelineArg*)user_data;

	twitux_debug (DEBUG_DOMAIN,
				  "Timeline response: %i",msg->status_code);

	arg->self->processing = FALSE;

	/* Timeout */
	arg->self->timeout_new ();

	/* Check response */
	if (!arg->self->check_http (msg->status_code)) {
		delete arg;
		return;
	}

	twitux_debug (DEBUG_DOMAIN, "Parsing timeline");

	/* Parse and set ListStore */
	if (twitux_parser_timeline (msg->response_body->data, msg->response_body->length)) {
		arg->self->signal_status_message (_("Timeline Loaded"));

		if (arg->new_timeline){
			if (arg->self->current_timeline)
				g_free (arg->self->current_timeline);
			arg->self->current_timeline = g_strdup (arg->new_timeline);
		}
	} else {
		arg->self->signal_status_message (_("Timeline Parser Error."));
	}

	delete arg;
}


/* On get user followers */
void
TwitterNetwork::cb_on_friends (SoupSession *session,
					 SoupMessage *msg,
					 gpointer     user_data)
{
	TwitterNetwork * self = (TwitterNetwork*)user_data;

	self->on_users(session, msg, TRUE);
}

void
TwitterNetwork::cb_on_followers (SoupSession *session,
					 SoupMessage *msg,
					 gpointer     user_data)
{
	TwitterNetwork * self = (TwitterNetwork*)user_data;

	self->on_users(session, msg, FALSE);
}

void TwitterNetwork::on_users (SoupSession * /*session*/,
							   SoupMessage *msg,
							   gboolean friends)
{
	UserListPtr    users;
		
	twitux_debug (DEBUG_DOMAIN,
				  "Users response: %i",msg->status_code);
	
	/* Check response */
	if (!check_http (msg->status_code))
		return;

	/* parse user list */
	twitux_debug (DEBUG_DOMAIN, "Parsing user list");

	users = twitux_parser_users_list (msg->response_body->data,
									  msg->response_body->length);

	/* check if it ok, and if it is a followers or following list */
	if (users && friends){
		/* Friends retrived */
		user_friends = users;
//		twitux_lists_dialog_load_lists (user_friends);
	} else if (users){
		/* Followers list retrived */
		user_followers = users;
//		twitux_message_set_followers (user_followers);
	} else {
		signal_status_message (_("Users parser error."));
	}
}


/* On get a image */
void
TwitterNetwork::cb_on_image (SoupSession * /*session*/,
							 SoupMessage *msg,
							 gpointer     user_data)
{
	ImageArg *arg = (ImageArg*)user_data;

	twitux_debug (DEBUG_DOMAIN,
				  "Image response: %i", msg->status_code);

	/* check response */
	if (arg->self->check_http (msg->status_code)) {
		/* Save image data */
		if (g_file_set_contents (arg->image->src,
								 msg->response_body->data,
								 msg->response_body->length,
								 NULL)) {
			/* Set image from file here (image_file) */
//			twitux_app_set_image (image->src,image->iter);
		}
	}

	g_free (arg->image->src);
	g_free (arg->image);
	delete arg;
}


/* On add a user */
void
TwitterNetwork::cb_on_add (SoupSession * /*session*/,
						   SoupMessage *msg,
						   gpointer     user_data)
{
	TwitterNetwork * self = (TwitterNetwork*)user_data;
	User::Ptr user;

	twitux_debug (DEBUG_DOMAIN,
				  "Add user response: %i", msg->status_code);
	
	/* Check response */
	if (!self->check_http (msg->status_code))
		return;

	/* parse new user */
	twitux_debug (DEBUG_DOMAIN, "Parsing new user");

	user = twitux_parser_single_user (msg->response_body->data,
									  msg->response_body->length);

	if (user) {
		self->user_friends->push_back( user );
		self->signal_status_message (_("Friend Added"));
	} else {
		self->signal_status_message (_("Failed to add user"));
	}
}


/* On remove a user */
void
TwitterNetwork::cb_on_del (SoupSession * /*session*/,
				   SoupMessage *msg,
				   gpointer     user_data)
{
	DelArg * arg = (DelArg*)user_data;

	twitux_debug (DEBUG_DOMAIN,
				  "Delete user response: %i", msg->status_code);

	if (arg->self->check_http (msg->status_code)) {		
		arg->self->signal_status_message (_("Friend Removed"));
	} else {
		arg->self->signal_status_message (_("Failed to remove user"));
	}
	
	if (user_data) {
		arg->self->user_friends->remove(arg->user);
	}
	delete arg;
}

void
TwitterNetwork::timeout_new (void)
{
	gint minutes;
	guint reload_time;

	if (timeout_id) {
		twitux_debug (DEBUG_DOMAIN,
					  "Stopping timeout id: %i", timeout_id);

		g_source_remove (timeout_id);
	}

	twitux_conf_get_int (twitux_conf_get (),
						 TWITUX_PREFS_TWEETS_RELOAD_TIMELINES,
						 &minutes);

	/* The timeline reload interval shouldn't be less than 3 minutes */
	if (minutes < 3) {
		minutes = 3;
	}

	/* This should be the number of milliseconds */
	reload_time = minutes * 60 * 1000;

	timeout_id = g_timeout_add (reload_time,
								TwitterNetwork::timeout,
								this);

	twitux_debug (DEBUG_DOMAIN,
				  "Starting timeout id: %i", timeout_id);
}

gboolean
TwitterNetwork::timeout (gpointer user_data)
{
	TwitterNetwork * self = (TwitterNetwork*)user_data;

	if (!self->current_timeline || self->processing)
		return FALSE;

	/* UI */
	self->signal_status_message (_("Reloading timeline..."));

	twitux_debug (DEBUG_DOMAIN,
				  "Auto reloading. Timeout: %i", self->timeout_id);

	self->processing = TRUE;
	self->get_data (self->current_timeline, cb_on_timeline, new TimelineArg(self, NULL));

	self->timeout_id = 0;

	return FALSE;
}


}
