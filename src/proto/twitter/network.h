/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 2007-2008 Daniel Morales <daniminas@gmail.com>
 * Copyright (C) 2009 Hubert Figuiere
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copied from Twitux - Authors: Daniel Morales <daniminas@gmail.com>
 *
 */

#ifndef __TWITUX_NETWORK_H__
#define __TWITUX_NETWORK_H__

#include <glib.h>
#include <libsoup/soup.h>

#include "microb/user.hpp"
#include "microb/protocol.hpp"
#include "proto/twitter/parser.h"

namespace MicroB {


struct TwituxImage;

class TwitterNetwork
	: public Protocol
{
public:
	TwitterNetwork(void);
	virtual ~TwitterNetwork(void);


    /* Verify user credentials */
	void login		(const char *username, 
									 const char *password);
    /* Logout current user */
	void logout 		(void);
    /* Post a new tweet */
	void post_status		(const gchar *text);
    /* Post a direct message to a follower */
	void send_message	(const gchar *frnd,
										 const gchar *text);
    /* Get and parse a timeline */
	void get_timeline 	(const gchar *url_timeline);
    /* Retrive a user timeline. If user is null, get
     * authenticated user timeline*/
	void get_user		(const gchar *username);
    /* Refresh current timeline */
	void refresh			(void);
    /* Get authenticating user's friends(following) */
	UserListPtr get_friends	(void);
    /* Get the authenticating user's followers */
	UserListPtr get_followers	(void);
    /* Get an image from servers */
	void get_image (const gchar *url_image) const;
//							   GtkTreeIter iter);
    /* Add a user to follow */
	void add_user		(const gchar *username);
    /* Remove a user */
	void del_user		(const User::Ptr & user);
    /* Networking */
	void stop		(void);
private:
	void get_data (const std::string &   url,
				  SoupSessionCallback    callback,
				   gpointer               data) const;
	void post_data (const std::string &    url,
					gchar                 *formdata,
					SoupSessionCallback    callback,
					gpointer               data) const;
	void parser_free_lists ();
	void timeout_new (void);
	static gboolean timeout			(gpointer user_data);
	gboolean	check_http 	(gint status_code) const;


/* libsoup callbacks */
	static void cb_on_login		(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	static void cb_on_post		(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	static void cb_on_message	(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	struct TimelineArg
	{
		TimelineArg(TwitterNetwork * _self, char * _new_timeline)
			: self(_self)
			, new_timeline(_new_timeline)
			{}
		~TimelineArg()
			{
				if(new_timeline)					
					g_free (new_timeline);
			}
		TwitterNetwork *self;
		char * new_timeline;
	};
	static void cb_on_timeline	(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	void on_users (SoupSession *session, SoupMessage *msg, gboolean friends);
	static void cb_on_friends	(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	static void cb_on_followers (SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	struct ImageArg
	{
		ImageArg(const TwitterNetwork * _self, TwituxImage * _image)
			: self(_self)
			, image(_image)
			{}
		const TwitterNetwork * self;
		TwituxImage * image;
	};
	static void cb_on_image		(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	static void cb_on_add		(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	struct DelArg
	{
		DelArg(TwitterNetwork * _self, const User::Ptr _user)
			: self(_self)
			, user(_user)
			{}
		TwitterNetwork * self;
		User::Ptr user;
	};
	static void cb_on_del		(SoupSession           *session,
										 SoupMessage           *msg,
										 gpointer               user_data);
	static void cb_on_auth		(SoupSession           *session,
										 SoupMessage           *msg,
										 SoupAuth              *auth,
										 gboolean               retrying,
										 gpointer               data);


	SoupSession			*soup_connection;
	UserListPtr          user_friends;
	UserListPtr          user_followers;
	gboolean             processing;
	gchar				*current_timeline;
	guint				 timeout_id;
	std::string          m_username;
	std::string          m_password;
};


}

G_BEGIN_DECLS

/* Twitter API */
#define TWITUX_API_REPLIES		"https://twitter.com/statuses/replies.xml"
#define TWITUX_API_DIRECT_MESSAGES	"https://twitter.com/direct_messages.xml"

#define TWITUX_API_POST_STATUS		"https://twitter.com/statuses/update.xml"
#define TWITUX_API_SEND_MESSAGE		"https://twitter.com/direct_messages/new.xml"

#define TWITUX_API_FOLLOWING		"https://twitter.com/statuses/friends.xml"
#define TWITUX_API_FOLLOWERS		"https://twitter.com/statuses/followers.xml"

#define TWITUX_API_FOLLOWING_ADD	"https://twitter.com/friendships/create/%s.xml"
#define TWITUX_API_FOLLOWING_DEL	"https://twitter.com/friendships/destroy/%s.xml"

#define TWITUX_API_LOGIN		"https://twitter.com/account/verify_credentials.xml"




G_END_DECLS

#endif /*  __TWITUX_NETWORK_H__ */
