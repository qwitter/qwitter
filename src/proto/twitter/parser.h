/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Copyright (C) 2007-2008 Daniel Morales <daniminas@gmail.com>
 * Copyright (C) 2009 Hubert Figuiere
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copied from Twitux - Authors: Daniel Morales <daniminas@gmail.com>
 *
 */
#ifndef __TWITUX_PARSER_H__
#define __TWITUX_PARSER_H__

#include <glib.h>

#include "microb/user.hpp"

namespace MicroB {

class TwitterNetwork;


/* Returns a liststore for the main treeview to show tweets */
gboolean twitux_parser_timeline (const gchar *data, 
								 gssize       length);

/* Returns a Glist with friends. Can be used to 
   build the friends menu, on direct messages dialog, etc.. */
UserListPtr twitux_parser_users_list (const gchar *data,
								 gssize       length);

/* Parse a xml user node. Ex: add/del users responses */
User::Ptr twitux_parser_single_user (const gchar *data,
									   gssize       length);

/* Restet the ID of the last tweet showed */
void parser_reset_lastid (void);

}

#endif /*  __TWITUX_PARSER_H__ */
