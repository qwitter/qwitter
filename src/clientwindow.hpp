/*
 * qwitter
 *
 * Copyright (C) 2009 Hubert Figuiere
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __QWITTER_CLIENTWINDOW_HPP_
#define __QWITTER_CLIENTWINDOW_HPP_

#include <gtkmm/actiongroup.h>
#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/notebook.h>
#include <gtkmm/uimanager.h>
#include <gtkmm/window.h>

#include "microb/accounts.hpp"

namespace qwitter {

class MicroBClient;
class MicroBContext;

class ClientWindow
  : public Gtk::Window
{
public:
  ClientWindow();
  ~ClientWindow();

  void add_tab(Gtk::Widget * w, const Glib::ustring & label);

private:

  MicroBAccounts::Ptr load_accounts();

  void cancel_message();
  void send_message();
  void reset_context();

  Glib::RefPtr<Gtk::UIManager>   m_manager;
  Glib::RefPtr<Gtk::ActionGroup> m_actions;
  Gtk::VBox                      m_root_content;
  Gtk::VBox                    * m_layout;
  Gtk::Notebook                * m_tabs;
  Gtk::Entry                   * m_input;

  MicroBClient                 * m_client;
  MicroBAccounts::Ptr            m_accounts;
  MicroBContext                * m_context;
};

}

#endif
